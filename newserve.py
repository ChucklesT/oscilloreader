import asyncio
import websockets
import time
from datetime import datetime

VALS_PER_SEC = 1000
VALS_PER_PAC = 50
COMM_INTERVAL = VALS_PER_PAC / VALS_PER_SEC

def give(filename):
  async def innerF(websocket, path):
    while True:
      with open(filename, 'r') as f:
          await websocket.send(str(VALS_PER_SEC))
          for line in f:
              vals = line.strip().split(',')
              packet = []
              for v in vals:
                  try:
                    packet.append(str(v))
                  except:
                    pass
                  if len(packet) == VALS_PER_PAC:
                      time.sleep(COMM_INTERVAL)
                      await websocket.send(','.join(packet))
                      packet = []

  return innerF


start_server = websockets.serve(give("sin.txt"), "localhost", 65433)
#start_server2 = websockets.serve(give("tan.txt"), "127.0.0.1", 65434)
#start_server3 = websockets.serve(give("square.txt"), "127.0.0.1", 65435)

asyncio.get_event_loop().run_until_complete(start_server)
#asyncio.get_event_loop().run_until_complete(start_server2)
#asyncio.get_event_loop().run_until_complete(start_server3)
asyncio.get_event_loop().run_forever()
