import asyncio
import websockets
import time
import wfdb

VALS_PER_SEC = 2000
VALS_PER_PAC = 50
COMM_INTERVAL = VALS_PER_PAC / VALS_PER_SEC

vals, desc = wfdb.rdsamp('data/mit/16273')

def give(filename):
  async def innerF(websocket, path):
    print(desc["comments"])
    while True:
      await websocket.send(str(VALS_PER_SEC))
      packet = []
      for v in vals:
        packet.append(str(v[0]))
        if len(packet) == VALS_PER_PAC:
          time.sleep(COMM_INTERVAL)
          await websocket.send(','.join(packet))
          packet = []

  return innerF


start_server = websockets.serve(give("ecgwav_noise.wav"), "127.0.0.1", 65435)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
